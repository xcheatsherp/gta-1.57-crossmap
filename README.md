## GTA 1.57 Crossmap

This crossmap was generated in about 30 minutes automatically.
I recommend that anyone creating their own should spend some time to write an updater and save themself some time.

AFAIK all mappings are accurate and it contains all natives used in the scripts.


**Usage is subject to the terms in the [licence](https://gitlab.com/xcheatsherp/gta-1.57-crossmap/-/blob/main/LICENSE)**
